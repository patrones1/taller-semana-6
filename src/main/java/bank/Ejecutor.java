package bank;

import bank.rules.*;

import java.util.ArrayList;
import java.util.List;

public class Ejecutor {

    public List<String> moverDinero(Transaccion transaccion) {

        List<String> reglasVioladas = new ArrayList<>();

        ReglaMontoCero reglaMontoCero = new ReglaMontoCero();
        String montoCeroViolada = reglaMontoCero.validarMontoCero(transaccion);

        if (!montoCeroViolada.isEmpty()) {

            reglasVioladas.add(montoCeroViolada);

        }

        ReglaMontoMaximo reglaMontoMaximo = new ReglaMontoMaximo();
        String montoMaximoViolada = reglaMontoMaximo.validarMontoMaximo(transaccion);

        if (!montoMaximoViolada.isEmpty()) {

            reglasVioladas.add(montoMaximoViolada);

        }

        ReglaActiva reglaActiva = new ReglaActiva();
        String activaViolada = reglaActiva.cuentaActiva(transaccion);

        if (!activaViolada.isEmpty()) {

            reglasVioladas.add(activaViolada);

        }

        ReglaMontoSoportado reglaMontoSoportado = new ReglaMontoSoportado();
        String montoSoportadoViolada = reglaMontoSoportado.validarMontoSoportado(transaccion);

        if (!montoSoportadoViolada.isEmpty()){

            reglasVioladas.add(montoSoportadoViolada);

        }

        ReglaChip reglaChip = new ReglaChip();
        String chipViolada = reglaChip.validar(transaccion);

        if (!chipViolada.isEmpty()) {

            reglasVioladas.add(chipViolada);

        }

        return reglasVioladas;

    }

}
