package bank;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        System.out.println("SOLID");

        Ejecutor ejecutor = new Ejecutor();

        Cuenta cuenta = new Cuenta();
        cuenta.activa = false;
        cuenta.monto = 200000.0;

        Transaccion transaccion = new Transaccion();
        transaccion.valor = 100.00;
        transaccion.origen = "CHIP";
        transaccion.cuenta = cuenta;

        List<String> reglasVioladas = ejecutor.moverDinero(transaccion);

        reglasVioladas.forEach(System.out::println);

    }

}
