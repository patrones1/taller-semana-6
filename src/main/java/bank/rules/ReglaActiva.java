package bank.rules;

import bank.Transaccion;

public class ReglaActiva {

    public String cuentaActiva(Transaccion transaccion){

        // cuenta activa

        if (!transaccion.cuenta.activa){

            return "Regla violada: Cuenta inactiva";

        }

        return "";

    }

}
