package bank.rules;

import bank.Transaccion;

public class ReglaChip {

    public String validar(Transaccion transaccion) {

        // Inventar 1 regla con los atributos existentes

        if (!transaccion.origen.equals("CHIP")){

            return "Regla violada: Origen diferente a chip";

        }

        return "";

    }

}
