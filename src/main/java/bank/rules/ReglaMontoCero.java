package bank.rules;

import bank.Transaccion;

public class ReglaMontoCero {

    public String validarMontoCero(Transaccion transaccion){

        // monto diferente a 0

        if (transaccion.valor == 0) {

            return "Regla violada: Monto igual a 0";

        }

        return "";

    }

}
