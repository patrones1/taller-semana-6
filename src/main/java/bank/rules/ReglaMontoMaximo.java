package bank.rules;

import bank.Transaccion;

public class ReglaMontoMaximo {

    public String validarMontoMaximo (Transaccion transaccion){

        // maximo a tranzar 100.000

        if (transaccion.valor > 100000){

            return "Regla violada: Monto superior a 100000";

        }

        return "";

    }

}
