package bank.rules;

import bank.Transaccion;

public class ReglaMontoSoportado {

    public String validarMontoSoportado (Transaccion transaccion) {

        // Monto soportado en cuenta

        if (transaccion.valor > transaccion.cuenta.monto) {

            return "Regla violada: Saldo insuficiente";

        }

        return "";

    }

}
